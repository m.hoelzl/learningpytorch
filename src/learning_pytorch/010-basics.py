# %%
import torch
import numpy as np
from pprint import pprint
from io import StringIO
from torch import dtype, Tensor


# %%
def pprint_indent(x, indent=0):
    stream = StringIO()
    pprint(x, stream=stream, width=120 - indent)
    lines = stream.getvalue().split('\n')
    if not lines:
        return
    first_line = lines[0]
    rest_lines = (' ' * indent + line for line in lines[1:])
    print(first_line)
    for line in rest_lines:
        print(line)


# %%
def describe(x):
    if isinstance(x, torch.Tensor):
        x_type = x.type()
    else:
        x_type = type(x)
    print(f'Type:       {x_type}')
    print(f'Shape/size: {x.shape}')
    print('Values:     ', end='')
    pprint_indent(x, indent=len('Values:     '))


# %%
describe(torch.Tensor(2, 3))
describe(torch.rand(2, 3))
describe(torch.randn(2, 3))
describe(torch.randint(5, (2, 3)))

# %%
describe(torch.zeros(2, 3))
_x = torch.ones(2, 3)
describe(_x)
_x.fill_(5)
describe(_x)

# %%
_x = torch.Tensor([[1, 2, 3],
                   [4, 5, 6]])
describe(_x)

# %%
_npy = np.random.rand(2, 3)
describe(_npy)

# %%
_x = torch.FloatTensor([[1, 2, 3],
                        [4, 5, 6]])
describe(_x)
describe(_x.long())

# %%
_x = torch.tensor([[1, 2, 3], [4, 5, 6]], dtype=torch.int64)
describe(_x)
describe(_x.float())

# %%
_x = torch.randn(2, 3)
describe(_x)

# %%
describe(torch.add(_x, _x))
describe(_x + _x)

# %%
_x = torch.arange(6)
describe(_x)

# %%
_x = _x.view(2, 3)
describe(_x)
describe(torch.sum(_x, dim=0))
describe(torch.sum(_x, dim=1))

# %%
describe(_x)
describe(torch.transpose(_x, 0, 1))

# %%
_x = torch.Tensor([[[1, 2], [3, 4]],
                   [[5, 6], [7, 8]]])
describe(_x)
describe(torch.transpose(_x, 0, 1))
describe(torch.transpose(_x, 0, 2))

# %%
_x = torch.arange(6).view(2, 3)
describe(_x)
describe(_x[:1, :2])

# %%
describe(_x[0, 1])
print(_x[0, 1].item())

# %%
_indices = torch.tensor([0, 2], dtype=torch.int64)
describe(torch.index_select(_x, dim=1, index=_indices))

# %%
_indices = torch.tensor([0, 1, 0, 1], dtype=torch.int64)
describe(torch.index_select(_x, dim=0, index=_indices))

# %%
_row_indices = torch.arange(2).long()
_col_indices = torch.LongTensor([0, 1])
describe(_x[_row_indices, _col_indices])

# %%
_row_indices = torch.LongTensor([[0, 0, 0], [1, 1, 1]])
_col_indices = torch.LongTensor([0, 1, 2])
describe(_x)
describe(_x[_row_indices, _col_indices])

# %%
_row_indices = torch.LongTensor([0, 1])
_col_indices = torch.LongTensor([[0, 0], [1, 1], [2, 2]])
describe(_x)
describe(_x[_row_indices, _col_indices])

# %%
_x = torch.arange(6).view(2, 3)
describe(_x)
describe(torch.cat([_x, _x], dim=0))
describe(torch.cat([_x, _x], dim=1))

# %%
describe(torch.stack([_x, _x]))

# %%
_x = torch.arange(6.).view(2, 3)
describe(_x)
_y = torch.tensor([[1., 2.], [3., 4.], [5., 6.]])
describe(_y)

# %%
describe(torch.mm(_x, _y))
describe(torch.mm(_y, _x))

# %%
# Gradients

_x1 = torch.tensor([[0., 1.], [2., 3.]], requires_grad=True)
_x2 = torch.tensor([[2., 1.], [3., 1]], requires_grad=True)
describe(_x)
_y = (_x1 + 2) * (_x2 + 5) + 3
describe(_y)
_z = _y.mean()
describe(_z)
_z.backward()
print('Gradient of x1: ', end='')
pprint_indent(_x1.grad, indent=len('Gradient of x1: '))
print('Gradient of x2: ', end='')
pprint_indent(_x2.grad, indent=len('Gradient of x2: '))
# print('Gradient of y:  ', end='')
# pprint_indent(_y.grad, indent=len('Gradient of y:  '))

# %%
# CUDA
print(f'Is CUDA available? {torch.cuda.is_available()}.')
_device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f"Device is '{_device}'.")

# %%
_x = torch.rand(3, 3).to(_device)
describe(_x)

# %%
_x = torch.tensor([[0., 1.], [2., 3.]])
_y = torch.unsqueeze(_x, 0)
describe(_y)

# %%
_z = torch.squeeze(_y, 0)
describe(_z)